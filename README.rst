=======================================
 paulusASol's translators for the Hurd
=======================================

This is a repository for my translators for the
`GNU Hurd <https://www.gnu.org/software/hurd/>`_.

Translators
===========

None

Translators (unfinished)
========================

random
------

A PRNG which should be cryptographically secure based on the Fortuna algorithm.
