/*
 * Copyright 2019 Paul Sonnenschein
 *
 * This file is part of my random translator.
 *
 * This random translator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This translator is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this translator. If not, see <https://www.gnu.org/licenses>
 */

 /*
  * Based upon the Fortuna algorithm from
  * Niels Ferguson, Bruce Schneier and Tadayoshi Kohno: Cryptography Engineering.
  * Indianapolis: Wiley Publishing, 2010
  */

#ifndef _FORTUNA_H_
#define _FORTUNA_H_

#include <gcrypt.h>

#include <limits.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

#define FORTUNA_KEY_ARRAY_SIZE (256 / (sizeof(uint64_t) * CHAR_BIT))
#define FORTUNA_COUNTER_ARRAY_SIZE (128 / (sizeof(uint64_t) * CHAR_BIT))
#define FORTUNA_POOL_COUNT (32)
#define FORTUNA_MAX_INPUT (32)
#define FORTUNA_MAX_OUTPUT (1<<20)

typedef struct {
    gcry_md_hd_t hash;
    size_t pool_size;
    pthread_mutex_t mutex;
} fortuna_pool;

typedef struct {
    uint64_t key[FORTUNA_KEY_ARRAY_SIZE];
    uint64_t counter[FORTUNA_COUNTER_ARRAY_SIZE];
    fortuna_pool pool[FORTUNA_POOL_COUNT];
    pthread_mutex_t mutex;
    uint64_t reseed_counter;
    struct timespec last_reseed;
    int hash_algorithm;
    int cipher_algorithm;
    void (*error_handler)(const char *func_name, const char *error_desc);
} fortuna_state;

bool fortuna_init(
    fortuna_state *state, int hash_algorithm, int cipher_algorithm,
    void (*error_handler)(const char *func_name, const char *error_desc));

bool fortuna_reseed(fortuna_state *state, char *seed, size_t seed_length);

bool fortuna_get_data(
    fortuna_state *state, char *output, size_t out_length, bool *fatal);

bool fortuna_add_event(
    fortuna_state *state, uint8_t event_source, unsigned int pool_id,
    char *data, size_t data_size);

#endif /* _FORTUNA_H_ */
