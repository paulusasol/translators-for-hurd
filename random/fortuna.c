/*
 * Copyright 2019 Paul Sonnenschein
 *
 * This file is part of my random translator.
 *
 * This random translator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This translator is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this translator. If not, see <https://www.gnu.org/licenses>
 */

/*
 * Based upon the Fortuna algorithm from
 * Niels Ferguson, Bruce Schneier and Tadayoshi Kohno: Cryptography Engineering.
 * Indianapolis: Wiley Publishing, 2010
 */

#define __STDC_WANT_LIB_EXT1__ 1

#include "fortuna.h"

#include <errno.h>
#include <string.h>

static const size_t FORTUNA_MIN_POOL_SIZE = 64;
static const long FORTUNA_RESEED_TIME_DELTA_NS = 100000000;

/*
 * Zero some memory.
 */
#if defined(__gnuc__) || defined (__clang__)
__attribute__((noinline))
#endif
static void fortuna_zero(char *address, size_t length) {
#ifdef __STDC_LIB_EXT__
    memset_s(address, length, 0, length);
#else
    explicit_bzero(address, length);
#endif
#if defined(__gnuc__) || defined (__clang__)
    asm("");
#endif
}

/*
 * Initialize a single pool.
 *
 * Compare 9.5.4 Initialization
 */
static bool fortuna_pool_init(fortuna_state *state, fortuna_pool *pool) {
    gcry_error_t error = gcry_md_open(
        &pool->hash, state->hash_algorithm, GCRY_MD_FLAG_SECURE);
    if (error != 0)
        return false;
    pool->pool_size = 0;
    return pthread_mutex_init(&pool->mutex, NULL) == 0;
}

/*
 * Initialize the PRNG
 *
 * Compare 9.4.1 Initialization and 9.5.4 Initialization
 */
bool fortuna_init(
    fortuna_state *state, int hash_algorithm, int cipher_algorithm,
    void (*error_handler)(const char *func_name, const char *error_desc)) {
    for (unsigned int i = 0; i < FORTUNA_KEY_ARRAY_SIZE; ++i)
        state->key[i] = 0;
    for (unsigned int i = 0; i < FORTUNA_COUNTER_ARRAY_SIZE; ++i)
        state->counter[i] = 0;
    for (unsigned int i = 0; i < FORTUNA_POOL_COUNT; ++i)
        if (!fortuna_pool_init(state, state->pool + i))
            return false;
    if (hash_algorithm == GCRY_MD_NONE)
        state->hash_algorithm = GCRY_MD_SHA256;
    else
        state->hash_algorithm = hash_algorithm;
    if (cipher_algorithm == GCRY_CIPHER_NONE)
        state->cipher_algorithm = GCRY_CIPHER_AES256;
    else
        state->cipher_algorithm = cipher_algorithm;
    state->reseed_counter = 0;
    state->last_reseed.tv_nsec = 0;
    state->last_reseed.tv_sec = 0;
    state->error_handler = error_handler;
    return pthread_mutex_init(&state->mutex, NULL) == 0;
}

/*
 * Helper function to increment the 128-bit counter.
 */
static void increase_counter(fortuna_state *state, uint64_t amount) {
    _Static_assert(
        sizeof(amount) == sizeof(state->counter[0]),
        "Unexpected size of counter elements");

    if (amount == 0)
        return;

    bool overflow = true;
    for (unsigned int i = 0; i < FORTUNA_COUNTER_ARRAY_SIZE && overflow; ++i) {
        uint64_t tmp = state->counter[i];
        state->counter[i] += amount;
        if (state->counter[i] >= tmp)
            overflow = false;
        else
            amount = 1;
    }
}

/*
 * Reseed the generator.
 *
 * This function should only be called for setting the initial state. Use
 * fortuna_add_event otherwise.
 *
 * Compare 9.4.2 Reseed
 */
bool fortuna_reseed(fortuna_state *state, char *seed, size_t seed_length) {
    if (pthread_mutex_lock(&state->mutex) != 0) {
        state->error_handler("Reseeding Fortuna", strerror(errno));
        return false;
    }

    _Static_assert(sizeof(state->key) == 32, "Key has unexpected size");
    gcry_md_hd_t hash_hd;
    gcry_error_t error;
    error = gcry_md_open(&hash_hd, state->hash_algorithm, GCRY_MD_FLAG_SECURE);
    if (error != 0) {
        state->error_handler("Reseeding Fortuna", gcry_strerror(error));
        return false;
    }
    gcry_md_write(
        hash_hd, (char *) state->key, sizeof(state->key));
    gcry_md_write(hash_hd, seed, seed_length);
    unsigned char *hash = gcry_md_read(hash_hd, 0);
    memcpy(state->key, hash, sizeof(state->key));

    increase_counter(state, 1);

    if (pthread_mutex_unlock(&state->mutex) != 0) {
        state->error_handler("Reseeding Fortuna", strerror(errno));
        return false;
    }
    return true;
}

/*
 * Generates pseudorandom blocks of data.
 *
 * Compare 9.4.3 Generate Blocks
 */
static bool fortuna_generate_blocks(fortuna_state *state,
                                    char *blocks, size_t num_bytes) {
    const int counter_size = sizeof(state->counter);
    const int key_size = sizeof(state->key);
    if (num_bytes % counter_size != 0)
        return false;

    gcry_cipher_hd_t cipher_hd;
    gcry_error_t error;
    error = gcry_cipher_open(
        &cipher_hd, state->cipher_algorithm, GCRY_CIPHER_MODE_ECB, 0);
    if (error != 0) {
        state->error_handler(
            "Generating pseudorandom blocks", gcry_strerror(error));
        return false;
    }

    error = gcry_cipher_setkey(cipher_hd, &state->key, key_size);
    if (error != 0) {
        state->error_handler(
            "Generating pseudorandom blocks", gcry_strerror(error));
        return false;
    }

    for (size_t i = 0; i < num_bytes; i += counter_size) {
        error = gcry_cipher_encrypt(
            cipher_hd, blocks + i, counter_size, &state->counter, counter_size);
        if (error != 0) {
            state->error_handler(
                "Generating pseudorandom blocks", gcry_strerror(error));
            return false;
        }

        increase_counter(state, 1);
    }
    return true;
}

/*
 * Generate pseudorandom bytes.
 *
 * Compare 9.4.4 Generate Random Data
 */
static bool fortuna_generate_data(fortuna_state *state, char *out, size_t out_bytes) {
    if (out_bytes > (FORTUNA_MAX_OUTPUT))
        return false;

    const int counter_size = sizeof(state->counter);
    const int key_size = sizeof(state->key);

    int less_than_out_bytes = (out_bytes / counter_size) * counter_size;
    bool success = fortuna_generate_blocks(state, out, less_than_out_bytes);
    if (!success)
        return false;

    char other_output[counter_size];
    success = fortuna_generate_blocks(state, other_output, counter_size);
    if (!success)
        return false;

    memcpy(out + less_than_out_bytes, other_output, out_bytes % counter_size);

    char new_key[key_size];
    success = fortuna_generate_blocks(state, new_key, key_size);
    if (!success)
        return false;

    memcpy(state->key, new_key, key_size);

    fortuna_zero(new_key, key_size);

    return true;
}

/*
 * Reseed if useful and get random bytes.
 *
 * Compare 9.5.5 Getting Random Data
 */
bool fortuna_get_data(
        fortuna_state *state, char *output, size_t out_length, bool *fatal) {
    bool alt_fatal;
    if (fatal == NULL)
        fatal = &alt_fatal;
    *fatal = false;
    if (out_length > FORTUNA_MAX_OUTPUT)
        return false;
    if (pthread_mutex_lock(&state->mutex) != 0) {
        state->error_handler("Getting pseudorandom Data", strerror(errno));
        *fatal = true;
        return false;
    }
    if (pthread_mutex_lock(&state->pool[0].mutex) != 0) {
        state->error_handler("Getting pseudorandom Data", strerror(errno));
        *fatal = true;
        return false;
    }
    size_t pool_size = state->pool[0].pool_size;
    if (pthread_mutex_unlock(&state->pool[0].mutex) != 0) {
        state->error_handler("Getting pseudorandom Data", strerror(errno));
        *fatal = true;
        return false;
    }

    struct timespec cur_time;
    clock_gettime(CLOCK_REALTIME, &cur_time);
    long time_delta = cur_time.tv_nsec - state->last_reseed.tv_nsec
        + (cur_time.tv_sec - state->last_reseed.tv_sec) * 1000000000;

    if (pool_size >= FORTUNA_MIN_POOL_SIZE
            && time_delta >= FORTUNA_RESEED_TIME_DELTA_NS) {
        ++state->reseed_counter;
        const int hash_size = 32;
        _Static_assert(sizeof(state->key) == 32, "Unexpected keysize");
        char new_seed[hash_size * FORTUNA_POOL_COUNT];
        int seed_length = 0;
        for (int i = 0; i < FORTUNA_POOL_COUNT; ++i) {
            fortuna_pool *pool = state->pool + i;
            if (pthread_mutex_lock(&pool->mutex) != 0) {
                state->error_handler("Getting pseudorandom data", strerror(errno));
                *fatal = true;
                return false;
            }
            if (state->reseed_counter % (1 << i) == 0) {
                unsigned char * hash = gcry_md_read(pool->hash, 0);
                memcpy(new_seed + seed_length, hash, hash_size);
                gcry_error_t error = gcry_md_open(
                    &pool->hash, state->hash_algorithm, GCRY_MD_FLAG_SECURE);
                if (error) {
                    state->error_handler(
                        "Getting pseudorandom data", gcry_strerror(error));
                    *fatal = true;
                    return false;
                }
                seed_length += hash_size;
            }
            if (pthread_mutex_unlock(&pool->mutex) != 0) {
                state->error_handler(
                    "Getting pseudorandom data", strerror(errno));
                *fatal = true;
                return false;
            }
        }
        fortuna_reseed(state, new_seed, seed_length);
        fortuna_zero(new_seed, sizeof(new_seed));
    }

    if (state->reseed_counter == 0) {
        if (pthread_mutex_unlock(&state->mutex) != 0) {
            state->error_handler("Getting pseudorandom data", strerror(errno));
            *fatal = true;
        }
        return false;
    } else if (!fortuna_generate_data(state, output, out_length)) {
        *fatal = true;
        return false;
    }
    if (pthread_mutex_unlock(&state->mutex) != 0) {
        state->error_handler("Getting pseudorandom data", strerror(errno));
        *fatal = true;
        return false;
    }
    return true;
}

/*
 * Add entropy to the entropy pools.
 *
 * Compare 9.5.6 Add an Event
 */
bool fortuna_add_event(
        fortuna_state *state, uint8_t event_source, unsigned int pool_id,
        char *data, size_t data_size) {
    if (pool_id >= FORTUNA_POOL_COUNT || data_size > FORTUNA_MAX_INPUT
            || data_size < 1)
        return false;
    fortuna_pool *pool = state->pool + pool_id;
    if (pthread_mutex_lock(&state->mutex) != 0) {
        state->error_handler("Adding entropy", strerror(errno));
        return false;
    }

    uint8_t data_size8 = data_size;

    gcry_md_write(pool->hash, &event_source, 1);
    gcry_md_write(pool->hash, &data_size8, 1);
    gcry_md_write(pool->hash, data, data_size);

    if (pthread_mutex_unlock(&state->mutex) != 0) {
        state->error_handler("Adding entropy", strerror(errno));
        return false;
    }
    return true;
}
