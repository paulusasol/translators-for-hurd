/*
 * Copyright 2019 Paul Sonnenschein
 *
 * This file is part of my random translator.
 *
 * This random translator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This translator is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this translator. If not, see <https://www.gnu.org/licenses>
 */

#define _GNU_SOURCE 1

#include "fortuna.h"

#include <hurd/trivfs.h>
#include <idvec.h>

#include <error.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/mman.h>

int trivfs_fstype = FSTYPE_MISC;
int trivfs_fsid = 0;

int trivfs_allow_open = O_READ | O_WRITE;
int trivfs_support_read  = 1;
int trivfs_support_write = 1;
int trivfs_support_exec  = 0;

fortuna_state state;

pthread_mutex_t pool_number_mutex;
uint8_t pool_for_source[256];

const uid_t ROOT_UID = 0;

const uint8_t ROOT_SOURCE = 128;
const uint8_t NO_USER_SOURCE = 129;
const uint8_t OTHER_USER_SOURCE_MIN = 130;
const uint8_t OTHER_USER_SOURCE_MAX = 255;

void on_fatal_error(const char *where, const char *what) {
    openlog("Random translator", LOG_PID | LOG_CONS, LOG_USER);
    syslog(LOG_CRIT | LOG_USER, "%s failed with %s", where, what);
    exit(EXIT_FAILURE);
}

void trivfs_modify_stat(struct trivfs_protid *credentials, io_statbuf_t *stat) {
    // TODO: do something here?
}

error_t trivfs_goaway(struct trivfs_control *control, int flags) {
    exit(EXIT_SUCCESS);
}

error_t trivfs_S_io_read(
        struct trivfs_protid *credentials,
        mach_port_t reply_port, mach_msg_type_name_t reply_type,
        data_t *data, mach_msg_type_number_t *data_length,
        loff_t offset, mach_msg_type_number_t amount) {
    if (!credentials)
        return EOPNOTSUPP;
    else if (! (credentials->po->openmodes & O_READ))
        return EBADF;

    bool success = true;

    if (amount > FORTUNA_MAX_OUTPUT) {
        amount = FORTUNA_MAX_OUTPUT;
    }

    if (amount > 0) {
        if (*data_length < amount)
            *data = mmap(0, amount, PROT_READ | PROT_WRITE, MAP_ANON, 0, 0);
        success = fortuna_get_data(&state, (char *) *data, amount, NULL);
        *data_length = amount;
    }

    return success ? 0 : EIEIO;
}

error_t trivfs_S_io_write(
        struct trivfs_protid *credentials,
        mach_port_t reply, mach_msg_type_name_t reply_type,
        data_t data, mach_msg_type_number_t data_length,
        loff_t offset, mach_msg_type_number_t *amount) {
    if (!credentials)
        return EOPNOTSUPP;
    else if (!(credentials->po->openmodes & O_WRITE))
        return EBADF;
    if (data_length > FORTUNA_MAX_INPUT)
        return EFBIG;
    struct idvec *uids = credentials->user->uids;
    uint8_t source;
    if (idvec_is_empty(uids))
        source = NO_USER_SOURCE;
    else if (idvec_contains(uids, ROOT_UID))
        source = ROOT_SOURCE;
    else {
        uid_t uid = uids->ids[0];
        uint8_t source = uid;
        source %= (OTHER_USER_SOURCE_MAX - OTHER_USER_SOURCE_MIN);
        source += OTHER_USER_SOURCE_MIN;
    }
    uint8_t pool;
    if (pthread_mutex_lock(&pool_number_mutex) != 0)
        return EIEIO;
    pool = pool_for_source[source]++;
    if (pthread_mutex_unlock(&pool_number_mutex) != 0)
        return EIEIO;
    if (!fortuna_add_event(&state, source, pool, (char *) data, data_length))
        return EIEIO;
    return 0;
}

error_t trivfs_S_io_readable(
        struct trivfs_protid *credentials,
        mach_port_t reply_port, mach_msg_type_name_t replytype,
        mach_msg_type_number_t *amount) {
    if (!credentials)
        return EOPNOTSUPP;
    else if (!(credentials->po->openmodes & O_READ))
        return EINVAL;
    else
        *amount = FORTUNA_MAX_OUTPUT;
    return 0;
}

int main(int argc, char *argv[]) {
    if (!mlockall(MCL_CURRENT | MCL_FUTURE)) {
        fprintf(stderr, "%s\n", "Failed to lock memory");
        return EXIT_FAILURE;
    }

    mach_port_t bootstrap_port;
    task_get_bootstrap_port(mach_task_self(), &bootstrap_port);
    if (bootstrap_port == MACH_PORT_NULL) {
        fprintf(stderr, "%s must be started as a translator\n", argv[0]);
        return EXIT_FAILURE;
    }

    fortuna_init(&state, GCRY_MD_NONE, GCRY_CIPHER_NONE, on_fatal_error);

    // TODO: setup entropy collectors
    exit(42);

    struct trivfs_control *control;
    error_t error = trivfs_startup(
        bootstrap_port, 0, NULL, NULL, NULL, NULL, &control);

    if (error) {
        fprintf(stderr, "%s\n", "Failed to start translator");
        return EXIT_FAILURE;
    }

    ports_manage_port_operations_multithread(
        control->pi.bucket, trivfs_demuxer, 100, 0, NULL);

    return EXIT_SUCCESS;
}
